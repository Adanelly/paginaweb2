import {Test} from './test';

export class Cita{
    posicion:number;
    id:number;
    contenido:string;
    fecha:string;
    hora:string;
    tema:string;
    temaCita: {
       id: any;
    }
}