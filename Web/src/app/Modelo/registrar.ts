export class Registrar{
    nombre:string;
    apellido:string;
    telefono:string;
    email:string;
    personaMoral:number;
    rfc:string;
    domicilioFiscal:string;
    password:string;
}