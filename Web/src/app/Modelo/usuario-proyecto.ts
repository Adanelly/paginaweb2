export class UsuarioProyecto{

    id: string;
    nombre: string;
    descripcion: string;
    estado: number;
    fechaEstimada: string;
    observaciones: string;
    posicion:number;
    sprints: {
        definicion: string;
        objetivos:{
            nombre: string;
            descripcion: string;
            estado: number;
        }
        
    }


}