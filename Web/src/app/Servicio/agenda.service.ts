import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Cita} from '../Modelo/Cita';
import {Observable, Subject} from 'rxjs';
import { Tema } from '../Modelo/Tema'; //este archivo esta diseñado para almacenar los datos que envie la api : id, tema
import {tap} from 'rxjs/operators';
import { APIUrl } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  constructor(private http:HttpClient) { }
  //Aqui hay dos url porque como yo hago las pruebas desde la misma red que la api, debo usar otra direccion
  
  private _refreshNeeded$ = new Subject<void>();
  private id:number;

  get refreshNeeded$(){
    return this._refreshNeeded$;
  }

  getTemas(){

    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo de tipo Tema
    return this.http.get<Tema[]>(APIUrl + 'api/agenda/cita/temas');
  }

  getCitas():Observable<any>{

    return this.http.get<Cita[]>(APIUrl + 'api/agenda/cita');
  }

  addCita(cita: Cita): Observable <Cita>{

    let json = (cita);
    return this.http.post<Cita>(APIUrl + 'api/agenda/cita', json)
    .pipe(tap(()=>{ //esta sentencia ejecuta la funcion de refresh 
      this._refreshNeeded$.next();
    }));

   }

   actualizarCita(cita : Cita):Observable <Cita>{
    let json = (cita);
    return this.http.put<Cita>(APIUrl + 'api/agenda/cita', json)
    .pipe(tap(()=>{
      this._refreshNeeded$.next();
    }));
   } 


   getId = function (id:number) 
   {
     this.id = id;

   }

   borrar():Observable <number>{
    let headers = new HttpHeaders().set( 'Authorization', 'Bearer ' +  localStorage.getItem("ACCESS_TOKEN"));
  

    return this.http.delete<number>(APIUrl + 'api/agenda/cita/'+ String(this.id), {headers: headers})
    .pipe(tap(()=>{
      this._refreshNeeded$.next();
    }));
   }
  
}
