import { Injectable } from '@angular/core';
import { Usuario } from '../Modelo/Usuario';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders,  HttpRequest, HttpHandler } from '@angular/common/http';
import { tap} from 'rxjs/operators'
import { JwtResponse } from '../Modelo/JwtResponse';
import { APIUrl } from 'src/environments/environment';
import {Respuesta} from '../Modelo/respuesta';
//import {JwtHelperService} from '@auth0/angular-Jwt';


@Injectable({
  providedIn: 'root'
})
export class AuthService{


 // helper = new JwtHelperService();

  constructor(private http:HttpClient) { }

  
  private token: string;


  private saveToken(token:string):void{
    localStorage.setItem("ACCESS_TOKEN",token);
    //console.log(token);
    this.token = token;
  }

  private getToken(): string{
    if(!this.token){
      this.token = localStorage.getItem("ACCESS_TOKEN");
    }

    return this.token;
  }

  login(usuario: Usuario): Observable <any>{
    let json = (usuario);
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post<any>(APIUrl + 'api/login', json, {headers: headers}).pipe(tap( 
     /* (respuesta:Respuesta)=>{
        console.log("asd");
      }
      ,*/
      (res:JwtResponse)=>{
        if(res){
          this.saveToken(res.token);
        }
      }
    ));
  }



  logout():void{
    this.token = '';
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("EXPIRES_IN");
  }


  
}


