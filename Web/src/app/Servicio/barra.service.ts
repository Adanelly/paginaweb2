import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Barra} from '../Modelo/Barra';
import { APIUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BarraService {

  constructor(private http:HttpClient) { }

  id:number;
  public getId(id:number):void{
    this.id = id;
  }

  getProgreso(){
    return this.http.get<Barra>(APIUrl + 'api/proyectos/progresos/' + String(this.id));
  }
  
}      
