import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormularioContacto} from '../Modelo/Formulario-contacto';
import { Observable } from 'rxjs';
import { APIUrl } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  constructor(private http:HttpClient) { }
addFormulario(formularioContacto: FormularioContacto): Observable <FormularioContacto>{
  let json = (formularioContacto);
  let headers = new HttpHeaders().set('Content-Type','application/json');

  return this.http.post<FormularioContacto>(APIUrl + 'api/send', json, {headers: headers});
}


}






