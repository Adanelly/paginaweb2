import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ClienteDatos} from '../Modelo/clienteDatos'
import { APIUrl } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DatosClienteService {

  constructor(private http:HttpClient) { }

    getDatos(){
     
      return this.http.get<ClienteDatos>(APIUrl + 'api/clientes/show');
    }
}
