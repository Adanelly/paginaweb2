import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent ,HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor{

  constructor( private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token: string = localStorage.getItem('ACCESS_TOKEN');
    console.log(token);
    let request = req;

    if (token) {
      request = req.clone({
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer ' + token
        })
      });
    }
    else{
      request = req.clone({
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      });
    }

    return next.handle(request).pipe(

     
      catchError((err: HttpErrorResponse) => {

        
    
        if (err.status === 401) {
          this.router.navigateByUrl('/login');
        }

        if (err.status === 200) {
          alert("AN UNEXPECTED ERROR OCURRED");
        }

        if(err.status === 403){
          this.tokenExpirado();
     
          
        }

        return throwError( err );

      })
    );
  }

  private tokenExpirado() {
    localStorage.removeItem('ACCESS_TOKEN');
    this.router.navigateByUrl('/login');
  }

}
