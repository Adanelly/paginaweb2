import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Registrar } from '../Modelo/registrar';
import { Observable } from 'rxjs';
import { APIUrl } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {

  constructor(private http:HttpClient) { }
  addRegistro(registro: Registrar): Observable <Registrar>{
    let json = (registro);
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post<Registrar>(APIUrl + 'api/clientes', json, {headers: headers});
  
   }
}
