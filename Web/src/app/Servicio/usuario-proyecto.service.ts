import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UsuarioProyecto} from '../Modelo/usuario-proyecto';
import {Barra} from '../Modelo/Barra';
import { APIUrl } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioProyectoService {

  constructor(private http:HttpClient) { }
  
  //url='http://lugar1.zapto.org:8080/';

  id:string;
  private getId(id:string):void{
    this.id = id;
  }


  getProyecto(){
  
    return this.http.get<UsuarioProyecto[]>(APIUrl + 'api/proyectos');
  }

  getProgreso(){
    
    return this.http.get<Barra>(APIUrl + 'api/proyectos/progresos/' + this.id);
  }
  
}
