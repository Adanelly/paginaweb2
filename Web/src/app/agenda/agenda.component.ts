import { Component, OnInit, ɵCodegenComponentFactoryResolver } from '@angular/core';
import { AgendaService } from '../Servicio/agenda.service';
import { Tema } from '../Modelo/Tema';
import { Cita } from '../Modelo/Cita';
import { Test } from '../Modelo/test';
import { escapeIdentifier } from '@angular/compiler/src/output/abstract_emitter';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  public detalles: boolean = false;
  public cambiar: boolean = true;
  public id: number;
  temp: string = "";
  agenda: Tema[];
  cita: Cita = new Cita();
  test: Test = new Test();
  temaCita: Test = new Test();
  citaArray: Cita[];
  mensaje = "";

  monthAct: number = new Date().getMonth();
  yearAct: number = new Date().getFullYear();


  constructor(private service: AgendaService) {

  }

  public create(): void {
    // console.log(this.cita);
    this.cita.temaCita = this.test;
    this.service.addCita(this.cita).subscribe(
      (response: Cita) => console.log(response)
    );
    alert("Su cita sera confirmada en unos momentos");
    this.cambiarVista();
  }

  /*public actualizar()  {

    this.cita.temaCita = this.temaCita;
    this.cita.id = this.citaArray[this.id].id;
  
    this.service.actualizarCita(this.cita).subscribe((response: Cita) => console.log(response));
    this.detalles = false;
  }*/

  public actualizar() {


    if (this.temaCita == null || this.citaArray[this.id].id == null || this.cita.contenido == null || this.cita.fecha == null || this.cita.hora == null) {
      console.log("faltan datos");
    } else {
      this.cita.temaCita = this.temaCita;
      this.cita.id = this.citaArray[this.id].id;

      this.service.actualizarCita(this.cita).subscribe(data => {

      }, err => {
        alert("pendejo");
      });
      this.detalles = false;
    }


  }

  public eliminar() {
    this.service.getId(this.citaArray[this.id].id);
    this.service.borrar().subscribe((response: any) => console.log(response));
  }

  private getCitas() {
    this.service.getCitas().subscribe(data => {
      this.citaArray = data;

      for (var i = 0; i < this.citaArray.length; i++) {
        this.citaArray[i].posicion = i;
      }
    });


  }


  public cambiarVista = function () {
    this.cambiar = this.cambiar ? false : true;

    if (this.cambiar) {
      console.log("true");
    }
    else {
      console.log("false");
    }
  }




  edit = function (id: number): void {
    this.id = id;
    this.detalles = true;


    //assigna el valor correspondiente de tema a la cita seleccionada
    for (var i = 0; i < this.agenda.length; i++)//compara el numero de tema de la cita seleccionada con los temas de la agenda 
    {
      if (this.citaArray[id].temaCita.id == this.agenda[i].id) {
        this.citaArray[id].tema = this.agenda[i].tema;
      }
    }

    console.log("editar");
    const arrastrar = document.getElementById('.detalles');


    arrastrar.addEventListener('dragstart', drag);


    function drag() {
      console.log("asdas");
      this.className += '  hold';
      setTimeout(() => (this.className = 'invisible'), 0);
    }


  }


  act = function () {

  }

  salir = function () {

    this.detalles = false;
  }




  ngOnInit(): void {


    this.service.refreshNeeded$.subscribe(() => {
      this.getCitas();
    });


    this.service.getTemas().subscribe(data => {
      this.agenda = data;
    });

    this.getCitas();





  }


  ngAfterContentInit(): void {


  }


}
