import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngresarComponent } from './ingresar/ingresar.component';
import { ContactoComponent } from './contacto/contacto.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { UsuarioProyectoComponent } from './usuario-proyecto/usuario-proyecto.component';
import { PerfilComponent } from './perfil/perfil.component';
import { InicioComponent } from './inicio/inicio.component';
import { AgendaComponent } from './agenda/agenda.component';
import { BarraProgresoComponent } from './barra-progreso/barra-progreso.component';


const routes: Routes = [
  {path:'test' , component: ContactoComponent},
  {path:'login', component: IngresarComponent },
  {path:'register', component: RegistrarComponent },
  {path: 'inicio', component:InicioComponent},
  {path: 'agenda', component:AgendaComponent},
  {path: 'uproyecto', component:UsuarioProyectoComponent},
  {path: 'uperfil', component:PerfilComponent},
  {path: 'progreso', component:BarraProgresoComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [IngresarComponent,ContactoComponent,InicioComponent,UsuarioProyectoComponent,PerfilComponent,AgendaComponent,BarraProgresoComponent]