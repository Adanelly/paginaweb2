import { Component, OnInit, AfterContentInit, ComponentFactoryResolver, ViewChild, Input } from '@angular/core';
import { IngresarComponent } from './ingresar/ingresar.component';
import { Assets } from 'src/environments/environment';
import { Chart } from 'chart.js';



import * as  $ from 'jquery';
import { Router } from '@angular/router';
import { BuiltinTypeName } from '@angular/compiler';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'Inblex';
    loggedOn = false;
    text: string = "CREATIVAS";
    assets: string = Assets;
    btn = false;

    constructor(private componentFactoryResolver: ComponentFactoryResolver, private router: Router) {

    }


    ngOnInit() {
        $(document).ready(function () {
            $('.leftmenutrigger').on('click', function (e) {
                $('.side-nav').toggleClass("open");
                $('#wrapper').toggleClass("open");
                e.preventDefault();
            });
        });


    }

    cerrarSesion = function () {

        localStorage.removeItem("ACCESS_TOKEN");
        window.location.reload();


    }

    sesionIniciada = function () {

        if (localStorage.getItem("ACCESS_TOKEN")) {
            this.loggedOn = true;
        }
        else {
            this.loggedOn = false;
        }

    }


    botonMenu = function () {

        this.btn = !this.btn;

        console.log(this.btn)
        if( $(window).scrollTop() < 20 && screen.height > 650 && this.btn == true && screen.width > 400)
        {
            
            $('.navbar').css({
                'height': 60 + 'px',
                'background-color': 'rgba(' + 0 + ',' + 0 + ',' + 0 + ',' + 0.5 + ')',
                'position': 'fixed',
                'transition-duration': 0.25 + 's'
            });
    
            $('.logo').css({
                'height': 30 + '%',
                'transition-duration': 0.25 + 's'
            });
    
            $('.menu').css({
                'height': 30 + '%',
                'transition-duration': 0.25 + 's'
            });
    
            $('.navbar-brand img').css({
                'width': 180 + 'px',
                'transition-duration': 0.25 + 's'
            });
    
            $('#navbarText').css({
                'height': 30 + '%',
                'transition-duration': 0.25 + 's'
            });
    
            $('.navbar-toggler').css({
                'margin-top': 4 + 'vh',
                'transition-duration': 0.25 + 's'
            });
    
            $('ul').css({
                'margin-top': 60 + 'px',
                'transition-duration': 0.25 + 's'
            });
    
            if (screen.width <= 992) {
    
    
                $('.navbar-toggler').css({
                    'margin-top': 3 + 'px',
                });
    
                $('.logo').css({
                    'height': 45 + 'px',
                    'transition-duration': 0.25 + 's'
                });
    
    
    
            } 
        }else if($(window).scrollTop() < 20 && this.btn == false && screen.width > 400){

            $('ul').css({
                'margin-top': 135 + 'px',
                'transition-duration': 0.25 + 's'
            });


            $('.navbar').css({
                'height': 0 + 'vh',
                'background-color': 'rgba(' + 0 + ',' + 0 + ',' + 0 + ',' + 0 + ')',
                'position': 'fixed',
                'transition-duration': 0.25 + 's'
            });

            $('.logo').css({
                'height': 100 + '%',
                'background': 'transparent',
                'transition-duration': 0.25 + 's'
            });

            $('#navbarText').css({
                'padding-top': 0,
                'height': 100 + '%',
                'background': 'transparent',
                'opacity': 1,
                'transition-duration': 0.25 + 's'
            });

            $('.navbar-brand img').css({
                'width': 300 + 'px',
                'align-items': 'center',
                'transition-duration': 0.25 + 's'
            });



            $('.menu').css({
                'height': 100 + '%',
                'transition-duration': 0.25 + 's'
            });

            if (screen.width <= 992) {



                $('.navbar-brand').css({
                    'margin-top': 2 + 'vh',
                    'transition-duration': 0.25 + 's'
                });

            } else {

                $('.navbar-brand').css({
                    'margin-bottom': 0 + 'vh',
                    'transition-duration': 0.25 + 's'
                });
            }
        }
    }

    ocultarMenu = function () {
        var element = document.getElementById("navbarText");
        //alert("aasdas");
        
        element.classList.remove('show');
        element.classList.add('collapse');
        
    }

    ngAfterContentInit(): void {

        this.sesionIniciada();

        let ruta;
        this.router.events.subscribe((function (event) {

            ruta = window.location.pathname;
            console.log(ruta);
            if (ruta == "/uperfil" || ruta == "/uproyecto" || ruta == "/agenda") {
                ocultarContenido();
            } else {
                mostrarContenido();
            }

        }));


        function mostrarContenido() {
            $('.block1').show();
            $('.block').show();
        }

        function ocultarContenido() {

            $('.block1').hide();
            $('.block').hide();
            // $('#footer').show();
        }


        function carrusel() {
            $('.5').hide();
            $('.4').hide();
            $('.3').hide();
            $('.2').hide();
            $('.1').show();
            /*$('.palabras').css({
                'transform': 'translateY(' +  110  + 'px)'
            });*/

            animacion1();



        }


        function animacion1() {
            $('.1 h1').css({
                'opacity': 0,
                'transition-duration': 2.0 + 's'
            });

            this.text = "INTELIGENTES";

            $('.1 h1').css({
                'opacity': 1,
                'transition-duration': 2.0 + 's'
            });
        }

        function animacion2() {
            $('.1 h1').css({
                'opacity': 1,
                'transition-duration': 2.0 + 's'
            });
        }

        function desplegarMenu() {


            if (screen.width < 400  || screen.height <= 650)  {
                $('.navbar').css({
                    'height': 60 + 'px',
                    'background-color': 'rgba(' + 0 + ',' + 0 + ',' + 0 + ',' + 0.5 + ')',
                    'position': 'fixed',
                    'transition-duration': 0.5 + 's'
                });

            } else if ($(window).scrollTop() >= 20) {

        
                $('.navbar').css({
                    'height': 60 + 'px',
                    'background-color': 'rgba(' + 0 + ',' + 0 + ',' + 0 + ',' + 0.5 + ')',
                    'position': 'fixed',
                    'transition-duration': 0.5 + 's'
                });

                $('.logo').css({
                    'height': 30 + '%',
                    'transition-duration': 0.5 + 's'
                });

                $('.menu').css({
                    'height': 30 + '%',
                    'transition-duration': 0.5 + 's'
                });

                $('.navbar-brand img').css({
                    'width': 180 + 'px',
                    'transition-duration': 0.5 + 's'
                });

                $('#navbarText').css({
                    'height': 30 + '%',
                    'transition-duration': 0.5 + 's'
                });

                $('.navbar-toggler').css({
                    'margin-top': 4 + 'vh',
                    'transition-duration': 0.5 + 's'
                });

                $('ul').css({
                    'margin-top': 60 + 'px',
                    'transition-duration': 0.5 + 's'
                });

                if (screen.width <= 992) {


                    $('.navbar-toggler').css({
                        'margin-top': 3 + 'px',
                    });

                    $('.logo').css({
                        'height': 45 + 'px',
                        'transition-duration': 0.5 + 's'
                    });
    
    

                } 

               /* if (screen.height <= 650) {
                    $('.navbar').css({
                        'height': 60 + 'px',
                    });
                  

                    $('#navbarText').css({
                        'height': 60 + 'px',
                        'transition-duration': 0.5 + 's'
                    });

                    $('.navbar-brand img').css({
                        'width': 150 + 'px',
                        'transition-duration': 0.5 + 's'
                    });
                }*/

            } else {
                $('ul').css({
                    'margin-top': 135 + 'px',
                    'transition-duration': 0.5 + 's'
                });


                $('.navbar').css({
                    'height': 0 + 'vh',
                    'background-color': 'rgba(' + 0 + ',' + 0 + ',' + 0 + ',' + 0 + ')',
                    'position': 'fixed',
                    'transition-duration': 0.5 + 's'
                });

                $('.logo').css({
                    'height': 100 + '%',
                    'background': 'transparent',
                    'transition-duration': 0.5 + 's'
                });

                $('#navbarText').css({
                    'padding-top': 0,
                    'height': 100 + '%',
                    'background': 'transparent',
                    'opacity': 1,
                    'transition-duration': 0.5 + 's'
                });

                $('.navbar-brand img').css({
                    'width': 300 + 'px',
                    'align-items': 'center',
                    'transition-duration': 0.5 + 's'
                });



                $('.menu').css({
                    'height': 100 + '%',
                    'transition-duration': 0.5 + 's'
                });

                if (screen.width <= 992) {



                    $('.navbar-brand').css({
                        'margin-top': 2 + 'vh',
                        'transition-duration': 0.5 + 's'
                    });

                } else {

                    $('.navbar-brand').css({
                        'margin-bottom': 0 + 'vh',
                        'transition-duration': 0.5 + 's'
                    });
                }

            }





        }


        $(document).ready(function () {

            desplegarMenu();

            
 
    
    var element = document.getElementById("navbarText");
  
            $('.bloques').click(function(){
                if(element.classList.contains('show')){
                   element.classList.remove('show');
                   element.classList.add('collapse');
                }
            });


            'use strict';
            //Calculate Body Padding//
            $('body').css('paddingTop', $('.navbar').innerHeight());
            //Scroll To Element//
            $('.navbar li a').click(function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: $('#' + $(this).data('scroll')).offset().top + 1
                }, 500);
            });
            //Add Active Class//
            $('.navbar li a').click(function () {
                // $('.navbar a').removeClass('active');
                // $(this).addClass('active');//
                $(this).addClass('active').parent().siblings().find('a').removeClass('active');
            });
            //Sync navbar With Sections//
            $(window).scroll(function () {

                $('.block').each(function () {

                    if ($(window).scrollTop() > $(this).offset().top) {

                        //console.log($(this).attr('id'));

                        var blockID = $(this).attr('id');

                        $('.navbar a').removeClass('active');

                        $('.navbar li a[data-scroll="' + blockID + '"]').addClass('active');

                    }
                });
            });
            //Scroll To Top Button//
            var scrolltotop = $(".scroll-top");
            $(window).scroll(function () {
                //--------------------------------------------------------//

                desplegarMenu();
                if ($(window).scrollTop() >= 20) {


                    /////////////////////////////////////////////////////
                } else {


                    



                    scrolltotop.fadeOut();
                }
            });
            //-------------------------------------------------------//10.127.127.1
            scrolltotop.click(function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 2000);
            });
            //  Fixed Menu //
            //console.log("The Inner Of Fixed Menu Is" + " " + $('.fixed-menu').innerWidth());
            $('.fixed-menu .fa-cog').click(function () {
                var fix = $(this).parent('.fixed-menu')
                fix.toggleClass('is-visible');
                if (fix.hasClass('is-visible')) {
                    $(fix).animate({
                        left: 0
                    });
                    $('body').animate({
                        // paddingLeft: '240px'
                    });
                } else {
                    $(fix).animate({
                        left: -$('.fixed-menu').innerWidth()
                    });
                    $('body').animate({
                        paddingLeft: '0'
                    });
                }
            });
        });

    }
}


