import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http'; 
import {FormsModule} from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgForm} from '@angular/forms';
import { InicioComponent } from './inicio/inicio.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { AgendaComponent } from './agenda/agenda.component';
//import { ContactoComponent } from './contacto/contacto.component';
import { FooterComponent } from './footer/footer.component';
import { ProductosComponent } from './productos/productos.component';
//import { IngresarComponent } from './ingresar/ingresar.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { UsuarioProyectoComponent } from './usuario-proyecto/usuario-proyecto.component';
import { PerfilComponent } from './perfil/perfil.component';
import { AgendaService } from './Servicio/agenda.service';
import { BarraProgresoComponent } from './barra-progreso/barra-progreso.component';
import { BarraService} from './Servicio/barra.service';


// Interceptors
import { InterceptorService } from '../app/Servicio/interceptor.service';

//graficos
import {ChartsModule} from 'ng2-charts';
import { from } from 'rxjs';
import { BarraTComponent } from './barra-t/barra-t.component';



@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ProyectosComponent,
    ServiciosComponent,
    AgendaComponent,
    //ContactoComponent,
    FooterComponent,
    //IngresarComponent,
    routingComponents,
    ProductosComponent,
    RegistrarComponent,
    UsuarioProyectoComponent,
    PerfilComponent,
    BarraProgresoComponent,
    BarraTComponent,
    
  
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
    
    
    ], //aqui van los servicios que usen la api
  bootstrap: [AppComponent]
})
export class AppModule { }
