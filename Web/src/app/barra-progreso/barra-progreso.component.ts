import { Component, OnInit, Input } from '@angular/core';
import { MultiDataSet, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';
import {Barra} from '../Modelo/Barra';
import {BarraService} from '../Servicio/barra.service';
import { from } from 'rxjs';
import { variable } from '@angular/compiler/src/output/output_ast';

//let  t: number= 50;

@Component({
  selector: 'app-barra-progreso',
  templateUrl: './barra-progreso.component.html',
  styleUrls: ['./barra-progreso.component.css']
})

export class BarraProgresoComponent implements OnInit{
  uBarra: Barra = null;
  total: number;
  sprint: number;

  @Input() id: number;
 

  public doughnutChartLabels: Label[] = ['Realizado','Falta'];
  public doughnutChartData: MultiDataSet  = [
    [0, 100-0]
   ];;
  /*public doughnutChartData: MultiDataSet = [
    [this.uBarra.progresoSprintActual, 100-this.uBarra.progresoSprintActual]
   ];*/
  public doughnutChartType: ChartType = 'doughnut';

  constructor(private barraService: BarraService) {
   
   }

  ngOnInit(): void {
   console.log(this.id);
    this.barraService.getId(this.id);
    this.barraService.getProgreso().subscribe(data => {
      this.uBarra = data;
      console.log(this.uBarra.progresoSprintActual);
      this.doughnutChartData = [[this.uBarra.progresoSprintActual, 100-this.uBarra.progresoSprintActual]];
      });
    
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}