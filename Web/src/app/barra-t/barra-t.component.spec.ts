import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarraTComponent } from './barra-t.component';

describe('BarraTComponent', () => {
  let component: BarraTComponent;
  let fixture: ComponentFixture<BarraTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarraTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarraTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
