import { Component, OnInit, Input } from '@angular/core';
import { MultiDataSet, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';
import {Barra} from '../Modelo/Barra';
import {BarraService} from '../Servicio/barra.service';
import { from } from 'rxjs';
import { variable } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-barra-t',
  templateUrl: './barra-t.component.html',
  styleUrls: ['./barra-t.component.css']
})
export class BarraTComponent implements OnInit {

  uBarra: Barra = null;
  total: number;
  sprint: number;

  @Input() id: number;
 

  public doughnutChartLabels: Label[] = ['Realizado','Falta'];
  public doughnutChartData: MultiDataSet  = [
    [0, 100-0]
   ];;
  /*public doughnutChartData: MultiDataSet = [
    [this.uBarra.progresoSprintActual, 100-this.uBarra.progresoSprintActual]
   ];*/
  public doughnutChartType: ChartType = 'doughnut';

  constructor(private barraService: BarraService) {
   
   }

  ngOnInit(): void {
   console.log(this.id);
    this.barraService.getId(this.id);
    this.barraService.getProgreso().subscribe(data => {
      this.uBarra = data;
      console.log(this.uBarra.progresoTotal);
      this.doughnutChartData = [[this.uBarra.progresoTotal, 100-this.uBarra.progresoTotal]];
      });
    
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
