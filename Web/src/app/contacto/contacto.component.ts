import { Component, OnInit } from '@angular/core';
import { ContactoService } from '../Servicio/contacto.service';
import {FormularioContacto} from '../Modelo/Formulario-contacto';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  formulario: FormularioContacto = new FormularioContacto();
  public hidden: boolean = true;
  n1: string;
  titulo: string;
  mensaje: string;

  constructor(private contactoService: ContactoService) { }



  public sendForm(): void {
    
     

      this.contactoService.addFormulario(this.formulario).subscribe(
        (response: FormularioContacto)=>{
          console.log(response)
          this.titulo = "Mensaje enviado exitosamente";
          this.mensaje ="Gracias por contactarnos, pronto nos pondremos en contacto con usted.";
          this.showPoopup();
          $('.errorText').css({
            'visibility': 'hidden',
          
          });
          this.formulario.nombreCliente = null, this.formulario.emailCliente = null, this.formulario.telefonoCliente = null, this.formulario.content = null;
        }, err => {
        
          console.log(err)
          this.titulo = "Error al enviar el mensaje";
          this.mensaje ="su mensaje no pudo ser enviado, por favor intentelo de nuevo mas tarde";
          this.showPoopup();
          $('.errorText').css({
            'visibility': 'hidden',
          
          });
     
        }
      );

  
   

    
     
   
   
      //alert("Datos insuficientes");
    //  $('input[type="text"], input[type="email"], textarea').focus(function(){});
    
   
   }


   public error(): void {

    $('.errorText').css({
      'visibility': 'visible',
     
      
    });
    $('.errorText').toggleClass('shake').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
    function() {
      $(this).removeClass('shake');
    });

  
    
    

    
    
   }

   
   public showPoopup = function () {

    this.hidden = false;

      $('.contenedor .overlay').css({
        'visibility': 'visible',
        'opacity': 1
      });
     
  }


   public hidePoopup = function () {
    this.hidden = true;

    $('.contenedor .overlay').css({
      'visibility': 'hidden',
      'opacity': 0
    });
}





  ngOnInit(): void {

   
   


  /*  $('document').ready(function(){
      $('input[type="text"], input[type="email"], textarea').focus(function(){
        var background = $(this).attr('id');
        $('#' + background + '-form').addClass('formgroup-active');
        $('#' + background + '-form').removeClass('formgroup-error');
      });
      $('input[type="text"], input[type="email"], textarea').blur(function(){
        var background = $(this).attr('id');
        $('#' + background + '-form').removeClass('formgroup-active');
      });
    
    function errorfield(field){
      $(field).addClass('formgroup-error');
      console.log(field);	
    }
    
    $("#waterform").submit(function() {
      var stopsubmit = false;
    
    if($('#name').val() == "") {
      errorfield('#name-form');
      stopsubmit=true;
    }
    if($('#email').val() == "") {
      errorfield('#email-form');
      stopsubmit=true;
    }
      if(stopsubmit) return false;
    });
        
    });*/
  
  }
  
  }  
