import { Component, OnInit } from '@angular/core';
import { AuthService } from '../Servicio/auth.service';
import { Usuario } from '../Modelo/Usuario';
import { JwtResponse } from '../Modelo/JwtResponse';
import { Router } from '@angular/router';



@Component({
    selector: 'app-ingresar',
    templateUrl: './ingresar.component.html',
    styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {
    enviado = false;
    Success = false;
    failure = false;
    mensaje = "";

 
    usuario: Usuario = new Usuario();

    constructor(private authService: AuthService, private router: Router) {

    }


    recargarPagina() {
        window.location.reload();
      }

   /* public validar(): void {

        this.authService.login(this.usuario).subscribe(response =>{
          
            this.failure = false;
            this.Success= true;
            this.router.navigateByUrl('/');
            //this.recargarPagina();
        });
        //console.log(this.usuario);
    }*/

    public validar(): void {

        this.authService.login(this.usuario).subscribe(data => {
            this.failure = false;
            this.Success= true;
            this.router.navigateByUrl('/uperfil');
            this.recargarPagina();
        }, err => {
            this.mensaje = "Ha ocurrido un error al ingresar";
            this.failure = true;
          });
        //console.log(this.usuario);
        this.enviado = true;
    }

    ngOnInit(): void {
       

    }



}


