import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Assets } from 'src/environments/environment';
import * as  $ from 'jquery';
import ScrollOut from 'scroll-out';



@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit, AfterContentInit {

  assets: string = Assets;
  texto = "Conocer más";
  btn = false;
  constructor() {

  }

    public animacion():void {
      if (!this.btn) {
        $('.dos').css({
          'margin-top': 55 + 'vh',
          'transition-duration': 0.5 + 's'
        });

        this.btn = true;
        this.texto = "Ocultar";

      } else {
        $('.dos').css({
          'margin-top': 35 + 'vh',
          'transition-duration': 0.5 + 's'
        });
        this.btn = false;
        this.texto = "Conocer más";
      }
    }

  ngOnInit(): void {

  }


  ngAfterContentInit(): void {
    var btn = false;

    //Called after ngOnInit when the component's or directive's content has been initialized.
    //Add 'implements AfterContentInit' to the class.


    /* $('.textos').css({
 
        
       'transform': 'translateY(' +  80  + 'px)'
 
 
     });*/

    ScrollOut({
      targets: ".contenedor div h4, .contenedor div p"

    });

    $('.contenedor div').css({
      //'transform': 'translateY(' +  (x- posicion * 8) + 'px)' + 'rotate3d('+ -posicion   +',' + posicion * 150 +',' + 20 + ',' + posicion + 'deg)'

    });



    //'transform': 'translateY(' +  (x- posicion * 8) + 'px)' + 'rotate3d('+ -posicion   +',' + posicion * 150 +',' + 20 + ',' + posicion + 'deg)'



    //});


  }



}
