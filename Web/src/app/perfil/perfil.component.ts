import { Component, OnInit } from '@angular/core';
import {DatosClienteService} from '../Servicio/datos-cliente.service';
import {ClienteDatos} from '../Modelo/clienteDatos';
import { from } from 'rxjs';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  clienteDatos: ClienteDatos;
  constructor(private datosClienteService: DatosClienteService) { }

  ngOnInit(): void {

    this.datosClienteService.getDatos().subscribe(data =>{
      this.clienteDatos = data;});
  }

}
