import { Component, OnInit } from '@angular/core';
import { Assets } from 'src/environments/environment';

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.css']
})
export class ProyectosComponent implements OnInit {

  assets : string = Assets;
  constructor() { }

  ngOnInit(): void {
  }

}
