import { Component, OnInit } from '@angular/core';
import { RegistroService } from '../Servicio/registro.service';
import { Registrar } from '../Modelo/registrar';



@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {
  registrar: Registrar = new Registrar();

  constructor(private registro: RegistroService) { }

  public created(): void {
    this.registro.addRegistro(this.registrar).subscribe((response: Registrar)=> console.log(response));

    /*this.registro.login(this.usuario).subscribe(response =>{
            console.log(response);
        });*/
   }
   
  ngOnInit(): void {
    
  }
  
}
