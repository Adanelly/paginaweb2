import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioProyectoComponent } from './usuario-proyecto.component';

describe('UsuarioProyectoComponent', () => {
  let component: UsuarioProyectoComponent;
  let fixture: ComponentFixture<UsuarioProyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuarioProyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuarioProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
