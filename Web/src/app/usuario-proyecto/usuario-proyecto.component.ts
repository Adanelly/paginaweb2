import { Component, OnInit , AfterContentInit} from '@angular/core';
import {UsuarioProyecto} from '../Modelo/usuario-proyecto';
import {UsuarioProyectoService} from '../Servicio/usuario-proyecto.service'

@Component({
  selector: 'app-usuario-proyecto',
  templateUrl: './usuario-proyecto.component.html',
  styleUrls: ['./usuario-proyecto.component.css']
})
export class UsuarioProyectoComponent implements OnInit {

  uProyecto: UsuarioProyecto[];
  estadoProyecto : string = "";
  public detalles:boolean = false;
  public id:number;

  constructor(private usuarioProyectoService: UsuarioProyectoService) {
  
   }

   mostrarDetalles = function (id:number) {
     this.detalles = true;
    this.id = id;
   
   }

  public salir = function () {
    this.detalles = false;
  }

  ngOnInit(): void {

  
    this.usuarioProyectoService.getProyecto().subscribe(data => {
      this.uProyecto = data;
    switch(this.uProyecto[0].estado)
    {
      case(0):
        this.estadoProyecto = "Pausado";
        break;

      case(1):
        this.estadoProyecto = "En desarrollo";
        break;

      case(2):
        this.estadoProyecto = "Terminado";
        break;
        
      default:
        break;
    } 

    for( var i = 0; i < this.uProyecto.length; i++)
      {
        this.uProyecto[i].posicion = i;
        console.log(this.uProyecto[i].sprints);
      }

    });

    
  }

  ngAfterContentInit(): void {
    


  }

}
